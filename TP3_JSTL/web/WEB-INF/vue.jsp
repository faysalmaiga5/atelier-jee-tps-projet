<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Formulaire</title>
        <link type="text/css" rel="stylesheet" href="css/form.css" />
    </head>
    <body>
        <div>
        <form method="post" action="form">
            <fieldset>
                <legend style="text-align: center;">   Inscription  </legend>
                <p>Merci de bien vouloir vous inscrire via ce formulaire.</p>

              
                <input type="text" id="nom" name="nom" placeholder="Nom" value="<c:out value="${param.nom}"/>" size="30" maxlength="20" />
                <span class="erreur" style="color: red;">${erreurs['nom']}</span>
                <br/>
          
                <input type="text" id="prenom" name="prenom" placeholder="Prénom" value="<c:out value="${param.prenom}"/>" size="30" maxlength="20" />
                <span class="erreur" style="color: red;">${erreurs['prenom']}</span>
                <br/>

                <input type="text" id="login" name="login" placeholder="Nom d'utilisateur" value="<c:out value="${param.login}"/>" size="30" maxlength="20" />
                <span class="erreur" style="color: red;">${erreurs['login']}</span>
                <br/>

                <input type="password" id="motdepasse" name="motdepasse" placeholder="Mot de passe" value="" size="30" maxlength="20" />
                <span class="erreur" style="color: red;">${erreurs['motdepasse']}</span>
                <br />

                <input type="password" id="confirmation" name="confirmation" placeholder="Confirmer votre mot de passe" value="" size="30" maxlength="20" />
                <br/>
                
                <input type="submit" value="Inscription" class="sansLabel" size="30" />
                <br/>
                
            </fieldset>
        </form>
        </div>

        <c:if test="${resultat!=null}">
            <div style="color: green;">
                ${resultat} 
            </div>
        </c:if>
    </body>
</html>