/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.models;

import javax.servlet.http.HttpServlet;

/**
 *
 * @author Faysal Maiga
 */
public class Utilisateur extends HttpServlet {
private String nom;
   private String prenom;
   private String nomDeCompte;
   private String monDePasse;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNomDeCompte() {
        return nomDeCompte;
    }

    public void setNomDeCompte(String nomDeCompte) {
        this.nomDeCompte = nomDeCompte;
    }

    public String getMonDePasse() {
        return monDePasse;
    }

    public void setMonDePasse(String monDePasse) {
        this.monDePasse = monDePasse;
    }
}
