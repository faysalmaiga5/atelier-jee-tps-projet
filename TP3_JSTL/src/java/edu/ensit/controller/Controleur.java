/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.controller;

import edu.ensit.metiers.Formulaire;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author PC
 */
@WebServlet(name = "form", urlPatterns = {"/form"})
public class Controleur extends HttpServlet {

    public static final String VUE          = "/WEB-INF/vue.jsp";

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        String resultat;
        Map<String, String> erreurs = new HashMap<String, String>();
        Formulaire form;
        try {
            form = new Formulaire(request);
            erreurs = form.getErreurs();
            if ( form.getErreurs().isEmpty() ) {
                resultat = "<h1>-- Vôtre profil --</h1>"
                        + "<h2>Nom : "+request.getParameter("nom")
                        + "<br> Prénom : "+request.getParameter("prenom")
                        + "<br> Nom de compte : "+request.getParameter("login")+"</h2>";
            }else
                resultat = null;
            
        request.setAttribute( "erreurs", erreurs );
        request.setAttribute( "resultat", resultat );
            this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        } catch (Exception ex) {
            Logger.getLogger(Controleur.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
