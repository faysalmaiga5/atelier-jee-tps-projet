/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.metiers;

import edu.ensit.models.Utilisateur;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author PC
 */

public class Formulaire extends HttpServlet {
  private Utilisateur utilisateur;
  private Map<String, String> erreurs = new HashMap<String, String>();

    public Formulaire(HttpServletRequest request) throws Exception {
        utilisateur = new Utilisateur();
        utilisateur.setNom(request.getParameter("nom"));
        utilisateur.setPrenom(request.getParameter("prenom"));
        utilisateur.setNomDeCompte(request.getParameter("login"));
        utilisateur.setMonDePasse(request.getParameter("motdepasse"));
        validationChamps(request.getParameter("nom"),request.getParameter("prenom"),request.getParameter("login"));
        validationMotsDePasse(request.getParameter("motdepasse"),request.getParameter("confirmation"));
        
    }
  
    public Utilisateur getUtiliateur() {
        return utilisateur;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }
  
  /**
 * Vérification des mots de passe saisis.
 */
private void validationMotsDePasse( String motdepasse, String confirmation ) throws Exception{
    if (motdepasse != null && motdepasse.trim().length() != 0 && confirmation != null && confirmation.trim().length() != 0) {
        if (!motdepasse.equals(confirmation)) {
            erreurs.put( "motdepasse", "Les mots de passe entrés sont différents, veuillez les saisir  les saisir de nouveau merci." );
        } else if (motdepasse.trim().length() < 3) {
            erreurs.put("motdepasse","Les mots de passe doivent contenir au moins 3 caractères.");
        }
    } else {
        erreurs.put("motdepasse","Merci de saisir et confirmer votre mot de passe.");
    }
}

/**
 * Vérification des champs.
 */
private void validationChamps( String nom, String prenom, String login ) throws Exception {
    if ( (nom != null && nom.trim().length() < 3) ) {
        erreurs.put("nom","Le nom doit contenir au moins 3 caractères." );
    }
    if ( (prenom!= null && prenom.trim().length() < 3) ) {
        erreurs.put("prenom","Le prénom doit contenir au moins 3 caractères.");
    }
    if ( (login!= null && login.trim().length() < 3) ) {
        erreurs.put("login","Le nom d'utilisateur doit contenir au moins 3 caractères.");
    }
}

}
