<%-- 
    Document   : accueil
    Created on : 4 avr. 2020, 04:41:18
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>Accueil</title>
    </head>
    <body>
        <% 
            String nom = (String) request.getParameter("nom");
            String prenom = (String) request.getParameter("prenom");
        %>
        <h3 style="color: red;">
            ${(nom.isEmpty() || prenom.isEmpty())? 'Il faut remplire les deux champs du formulaire !':''}
        </h3>
        <form action="travail" method="post" >
            <fieldset>
                <legend>Données utilisateur</legend>
                <input type="text" placeholder="Prénom" name="prenom" id="prenom" value="${prenom.isEmpty()? '':prenom}">
                <input type="text" placeholder="Nom" name="nom" id="nom" value="${nom.isEmpty()? '':nom}">                
                <input type="submit" value="OK">
            </fieldset>
        </form>
    </body>
</html>