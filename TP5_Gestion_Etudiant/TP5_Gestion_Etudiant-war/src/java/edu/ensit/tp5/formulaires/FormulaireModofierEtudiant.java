/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.tp5.formulaires;

import edu.ensit.tp5.modele.entities.NiveauEtude;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PC
 */
public class FormulaireModofierEtudiant {
    private String id;
    private String nom;
     private Date dateNaissance;
     private NiveauEtude niveauEtude;
     private Map<String,String> erreurs = new HashMap<String, String>();
     
     public FormulaireModofierEtudiant(String id,String nom, String dateNaissance, String niveauEtude) {
         this.id=id;
        try {
             this.setNom(nom);
        } catch (Exception e) {
            erreurs.put("nom", e.getMessage());
        }
        try {
            this.setDateNaissance(dateNaissance);
        } catch (EtudiantException ex) {
            erreurs.put("dateNaissance", ex.getMessage());
        }
        this.setNiveauEtude(niveauEtude);
     }
     
     private void setNom(String nom) throws EtudiantException{
         if(nom ==null || nom.trim().length() == 0){
            throw new EtudiantException("Nom complet doit etre renseigné");
        }
        this.nom = nom.trim();
    }
       public String getNom() {
        return nom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Date getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) throws EtudiantException{
        try {
            this.dateNaissance = new SimpleDateFormat("yyyy-MM-dd").parse(dateNaissance);
        } catch (ParseException ex) {
            throw new EtudiantException("Date de Naissance doit etres bien formé");
        }
        
    }
    
    public NiveauEtude getNiveauEtude() {
        return niveauEtude;
    }

    public void setNiveauEtude(String niveauEtude) {
        this.niveauEtude = NiveauEtude.valueOf(niveauEtude);
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public void setErreurs(Map<String, String> erreurs) {
        this.erreurs = erreurs;
    }
}
