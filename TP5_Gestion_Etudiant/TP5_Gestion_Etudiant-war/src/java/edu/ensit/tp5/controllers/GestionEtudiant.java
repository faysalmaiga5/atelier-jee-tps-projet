/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.tp5.controllers;

import edu.ensit.tp5.formulaires.FormulaireAjoutEtudiant;
import edu.ensit.tp5.formulaires.FormulaireModofierEtudiant;
import edu.ensit.tp5.modele.entities.Etudiant;
import edu.ensit.tp5.modele.entities.NiveauEtude;
import edu.ensit.tp5.modele.session.EtudiantFacade;
import java.io.IOException;
import java.util.Date;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author PC
 */
@WebServlet(name = "GestionEtudiant", urlPatterns = {"/GestionEtudiant"})
public class GestionEtudiant extends HttpServlet {

   
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    @EJB
    EtudiantFacade etudiantFacade;
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         //envoyer la liste des etuidiants
        //System.out.println(etudiantFacade.findAll());
        request.setAttribute("listeEtudiants", etudiantFacade.findAll());
        System.out.println(etudiantFacade.count());
        System.out.println(etudiantFacade.findAll());
        request.setAttribute("niveauxEtude", NiveauEtude.getNiveauEtude());
        
        System.out.println(NiveauEtude.getNiveauEtude());
        
        //on se dirige vers la vue gestionEtudiants.jsp
        request.getRequestDispatcher("WEB-INF/gestionEtudiants.jsp").forward(request, response);
        
    }

    
 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      FormulaireAjoutEtudiant formulaireAjoutEtudiant = new FormulaireAjoutEtudiant(
                request.getParameter("nom"), 
                request.getParameter("dateNaissance"), 
                request.getParameter("niveauEtude"));
        
        if(formulaireAjoutEtudiant.getErreurs().isEmpty()){
            Etudiant etudiant=new Etudiant();
            etudiant.setNom(formulaireAjoutEtudiant.getNom());
            etudiant.setDatenaissance(formulaireAjoutEtudiant.getDateNaissance());
            etudiant.setNiveau(formulaireAjoutEtudiant.getNiveauEtude());
            etudiantFacade.create(etudiant);
        }
        
      
        request.setAttribute("formulaireAjoutEtudiant", formulaireAjoutEtudiant);
        request.setAttribute("listeEtudiants", etudiantFacade.findAll());
        request.setAttribute("niveauxEtude", NiveauEtude.getNiveauEtude());
        request.getRequestDispatcher("WEB-INF/gestionEtudiants.jsp").forward(request, response);
    }
}
