<%@page import="javax.ejb.EJB"%>
<%@page import="edu.ensit.tp5.modele.session.EtudiantFacade"%>
<%@page import="edu.ensit.tp5.modele.entities.Etudiant"%>
<%@page import="edu.ensit.tp5.formulaires.FormulaireModofierEtudiant"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestion Etudiants</title>
    </head>
    <body>
        <div style="background-color: red;">
            <c:if test="${!empty formulaireAjoutEtudiant.erreurs}">
                <h2>Erreur d'ajout d'Etudiant</h2>
            </c:if>
                <c:if test="${!empty formulaireAjoutEtudiant.erreurs['nom']}">
                <h5>Erreur Nom : ${formulaireAjoutEtudiant.erreurs['nom']}</h5>
            </c:if>
            <c:if test="${!empty formulaireAjoutEtudiant.erreurs['dateNaissance']}">
                <h5>Erreur Date de Naissance : ${formulaireAjoutEtudiant.erreurs['dateNaissance']}</h5>
            </c:if>
        </div>
         <form action="GestionEtudiant" method="POST">
            <input type="text" name="nom" placeholder="Nom Complet" />
            <input type="date" name="dateNaissance" placeholder="jj/mm/aaaa" />
            <select name="niveauEtude" style="width: 100px">
                <c:forEach var="niveau" items="${niveauxEtude}">
                    <option value="${niveau}">${niveau}</option>
                </c:forEach>
            </select>
            <input type="submit" value="Ajouter Etudiant" />
        </form>
        <br>
     <form action="GestionEtudiant" method="POST">
    <table border="1">
            <tr>
                <th>Nom</th>
                <th>Date De Naissance</th>
                <th>Niveau d'etude</th>
                <th><center>Modifier ou Supprimer</center></th>
            </tr>
            <c:forEach var="etudiant" items="${listeEtudiants}">
                <tr>
                <td>${etudiant.nom}</td>
                <td>${etudiant.datenaissance}</td>
                <td>${etudiant.niveau}</td>
                <td>
                    <center>
                        <input type="button" id="id"   style="color: green;" onClick="modifier()" value="Modifier" />
                        <input type="button" id="id" style="color: red;" value="Supprimer" />
                    </center>
                </td>
            </tr>
            </c:forEach>
        </table>
        </form>
    </body>
</html>
