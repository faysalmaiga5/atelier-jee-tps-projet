package edu.ensit.tp5.modele.entities;

import edu.ensit.tp5.modele.entities.NiveauEtude;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-04-29T21:32:44")
@StaticMetamodel(Etudiant.class)
public class Etudiant_ { 

    public static volatile SingularAttribute<Etudiant, Date> datenaissance;
    public static volatile SingularAttribute<Etudiant, Long> id;
    public static volatile SingularAttribute<Etudiant, String> nom;
    public static volatile SingularAttribute<Etudiant, NiveauEtude> niveau;

}