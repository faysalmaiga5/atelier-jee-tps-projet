/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ensit.tp5.modele.entities;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author PC
 */
public enum NiveauEtude {
    GInfo_1, GInfo_2, GInfo_3;

     public static List<String> getNiveauEtude() {
        List<String> l = new LinkedList<String>();
        for(NiveauEtude niveauEtude :NiveauEtude.values()){
            l.add(niveauEtude.toString());
        }
        return l;
    }

}