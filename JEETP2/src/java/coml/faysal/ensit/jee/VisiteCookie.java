/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coml.faysal.ensit.jee;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author PC
 */
@WebServlet(name = "VisiteCookie", urlPatterns = {"/VisiteCookie"})
public class VisiteCookie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            Cookie[] mesCookies = request.getCookies();
            int nbv= 0;
            // si il y a deja le cookie
            if(mesCookies!=null){
                for (Cookie c : mesCookies) {
                    if(c.getName().equals("visites"))
                        nbv = Integer.parseInt(c.getValue());
                }
            }
            //dans le contaire on va le creer
            Cookie cookie=new Cookie("visites", String.valueOf(++nbv));
            cookie.setMaxAge(60*60*24*365);
            response.addCookie(cookie);
            String msg;
            // si c est la premeire visite
            if(nbv == 1){
                msg = "Bienvenue C est votre premiere visite pour une durée d une anee";
            }else{// sinon on affaiche le nombrre de viste et on affiche la page
                msg="Le nombre de viste est "+nbv;
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet VisiteCookie </title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet VisiteCookie at avec Mr Ramzi Tp2 ex1 </h1>");
                out.println("<h2>" +msg+ "</h2>");
                out.println("</body>");
                out.println("</html>");
            }
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
