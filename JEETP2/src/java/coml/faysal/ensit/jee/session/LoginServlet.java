/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coml.faysal.ensit.jee.session;

import coml.faysal.ensit.jee.session.bundles.Authentification;
import coml.faysal.ensit.jee.session.model.Utilisateur;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
/**
 *
 * @author PC
 */
@WebServlet(name = "login", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    
    //Declarfer des constantes pour representer les parameter(les champàs du formulaire donc)
     //exemple
   // private static String PARAM_LOGIN = "login";
   // private static String PARAM_MOTDEPASSE = "motdepasse";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession(false);//on recupere la session et la valeur false veut tout simplement dire que si 
            //la session n existe elle serai pas non plus crée ici
            //on inclus maintenat l entet
            request.getRequestDispatcher("WEB-INF/include/header.html").include(request, response);
            //si l'utisateur est deja connecté
            if (session != null && session.getAttribute("utilisateur") != null) {
                out.println("<h1> Vous etes deja coinnecté</h1>");
            } else {
                request.getRequestDispatcher("WEB-INF/include/login_form.html").include(request, response);
            }
            request.getRequestDispatcher("WEB-INF/include/footer.html").include(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            HttpSession session = request.getSession();
            //inclusion de l entete
            request.getRequestDispatcher("WEB-INF/include/header.html").include(request, response);
            //si l'utilmisateur est connecté
           // if (request.getParameter("nom").contains("user") && request.getParameter("motdepasse").contains("user")) {
            if(Authentification.isA(request.getParameter("nom"),request.getParameter("motdepasse"))){
                Utilisateur utilisateur = new Utilisateur();
                utilisateur.setNom(request.getParameter("nom"));
                utilisateur.setMotdepasse(request.getParameter("motdepasse"));
                session.setAttribute("utilisateur", utilisateur);
                out.println("<h1> Bienvenue  " + utilisateur.getNom().toUpperCase() + " Vous etes  connecte</h1>");
            } else {
                out.println("<p>Les parametres saisioes sont invalides !!!!!</p>");
                request.getRequestDispatcher("WEB-INF/include/login.html").include(request, response);
            }
            request.getRequestDispatcher("WEB-INF/include/footer.html").include(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
