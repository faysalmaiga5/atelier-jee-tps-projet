/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coml.faysal.ensit.jee.session.model;

/**
 *
 * @author PC
 */
public class Utilisateur {
    private String nom;
    private String motdepasse;

    public Utilisateur(String nom, String motdepasse) {
        this.nom = nom;
        this.motdepasse = motdepasse;
    }

    public Utilisateur() {
    }
    

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }
    
    
    
}
