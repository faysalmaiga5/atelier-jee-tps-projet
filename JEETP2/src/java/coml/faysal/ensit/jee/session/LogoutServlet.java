/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coml.faysal.ensit.jee.session;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author PC
 */
@WebServlet(name = "logout", urlPatterns = {"/LogoutServlet"})
public class LogoutServlet extends HttpServlet {

  
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            response.setContentType("text/html;charset=UTF-8");
            try (PrintWriter out = response.getWriter()) {
            request.getRequestDispatcher("WEB-INF/include/header.html").include(request, response);
            HttpSession session = request.getSession(false);
            if (session != null && session.getAttribute("utilisateur") != null) {
                request.getSession().invalidate();
                out.println("<h1> Vous etes  deconnecté</h1>");
            } else {
                out.println("<p> Vous etes  deja deconnecté</p>");
            }
            request.getRequestDispatcher("WEB-INF/include/footer.html").include(request, response);
        }
    }
}
